import random
import string
from fastapi import FastAPI, Form
from fastapi.responses import HTMLResponse, RedirectResponse
import requests
from urllib.parse import urlencode, urlsplit, parse_qsl
from jinja2 import Environment, FileSystemLoader, select_autoescape
import pkce



env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)


app = FastAPI()


def random_string(length):
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=length))

code_verifier, code_challenge = pkce.generate_pkce_pair()
new_state = random_string(10)

# print("Code Verifier", code_verifier)
# print("Code Challenge", code_challenge)
# print("State", new_state)

@app.get("/login", response_class=RedirectResponse)
def getroot():
    return "https://auth.tesla.com/oauth2/v3/authorize?" + urlencode(
        {
            "client_id": "ownerapi",
            "code_challenge": code_challenge,
            "code_challenge_method": "S256",
            "redirect_uri": "https://auth.tesla.com/void/callback",
            "response_type": "code",
            "scope": "openid email offline_access",
            "state": new_state,
        }
    )


@app.get("/", response_class=HTMLResponse)
def tokenpage():
    template = env.get_template("main.html")
    return template.render()


@app.post("/token")
def post_root(url: str = Form(...)):
    params_dict = dict(parse_qsl(urlsplit(url).query))
    code = params_dict["code"]
    token_response = requests.post(
        "https://auth.tesla.com/oauth2/v3/token",
        json={
            "grant_type": "authorization_code",
            "client_id": "ownerapi",
            "code": code,
            "code_verifier": code_verifier,
            "redirect_uri": "https://auth.tesla.com/void/callback",
        },
    )

    return token_response.json()
