from invoke import task

@task
def serve(c):
    c.run('uvicorn main:app --reload')